﻿using System;
using System.Security.Cryptography.X509Certificates;
using Hermes.Aggregates.Items;
using Hermes.Aggregates.Statuses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Hermes.Aggregates.Test
{
    [TestFixture]
    class ItemTests
    {
        #region Create Item Tests
        [Test]
        public void CreateItemTest_PassedNameIsSet()
        {
            //Assert Item creation
            var item = Item.Create("testItem");
            Assert.AreEqual(item.Name, "testItem");
        }

        [Test]
        public void CreateItemTest_GenerateId()
        {
            var item = Item.Create("testItem");
            Assert.IsInstanceOf(typeof(Guid), item.AggregateId);
        }

        [Test]
        public void CreateItem_AggregateVersionIsOne()
        {
            var item = Item.Create("testItem");
            Assert.AreEqual(1, item.AggregateVersion);
        }

        [Test]
        public void CreateItemTest_DescriptionIsSetIfProvided()
        {
            var item = Item.Create("testItem", "A Test Item");
            Assert.AreEqual("A Test Item", item.Description);
        }

        [Test]
        public void CreateItem_EventCreated()
        {
            var item = Item.Create("testItem", "A Test Item");
            Assert.IsNotEmpty(item.UncommittedEvents);
            Assert.AreEqual(1, item.UncommittedEvents.Count);
            Assert.AreEqual("Item Created", item.UncommittedEvents[0].Type);
        }
        #endregion

        #region Modify Item Tests
        [Test]
        public void InactivateItemTest()
        {
            //Arrange
            var item = Item.Create("testItem");
            var initialStatus = item.Status;

            //Act
            item.Inactivate();

            //Assert
            Assert.AreEqual(ItemStatus.Inactive, item.Status);
            Assert.AreNotEqual(initialStatus, item.Status);
        }

        [Test]
        public void CreateAndThenInactivateItem_ItemStatusChangedEventCreated()
        {
            //Arrange
            var item = Item.Create("testItem");
            var initialStatus = item.Status;

            //Act
            item.Inactivate();

            //Assert
            Assert.AreEqual(2, item.UncommittedEvents.Count);
            Assert.AreEqual("Item Status Changed", item.UncommittedEvents[1].Type);
        }

        [Test]
        public void ActivateItemTest()
        {
            //Arrange
            var item = Item.Create("testItem");
            item.Inactivate();
            var initialStatus = item.Status;

            //Act
            item.Activate();

            //Assert
            Assert.AreEqual(ItemStatus.Active, item.Status);
            Assert.AreNotEqual(initialStatus, item.Status);
        }

        [Test]
        public void UpdateItemDescription()
        {
            //Arrange
            var item = Item.Create("testItem");

            //Act
            item.UpdateItemDescription("This is a test description of a test item");

            //Assert
            Assert.AreEqual("This is a test description of a test item", item.Description);
        }

        [Test]
        public void UpdateItemDescription_NullParameter_DoesNotUpdate()
        {
            //Arrange
            var item = Item.Create("testItem", "This is a test description of a test item");

            //Act
            item.UpdateItemDescription(null);

            //Assert
            Assert.AreEqual("This is a test description of a test item", item.Description);
        }
        #endregion
    }
}
