﻿using Hermes.Aggregates.Items;
using Hermes.Aggregates.Orders;
using Hermes.Aggregates.Statuses;
using NUnit.Framework;

namespace Hermes.Aggregates.Test
{
    [TestFixture]
    class PickingOrderTests
    {
        #region Create Picking Order Tests
        [Test]
        public void CreatePickingOrder()
        {
            //Arrange & Act
            var pickingOrder = PickingOrder.Create();
            //Assert
            Assert.IsInstanceOf<PickingOrder>(pickingOrder);
        }

        [Test]
        public void WhenPickingOrderIsCreated_StatusSetToDraft()
        {
            //Arrange & Act
            var pickingOrder = PickingOrder.Create();

            //Assert
            Assert.AreEqual(OrderStatus.Draft, pickingOrder.Status);
        }
        #endregion

        #region Modify Picking Order Tests
        [Test]
        public void ReleaseOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();

            //Act
            pickingOrder.Release();

            //Assert
            Assert.AreEqual(OrderStatus.Open, pickingOrder.Status);
        }

        [Test]
        public void CancelDraftOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();

            //Act
            pickingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, pickingOrder.Status);
        }

        [Test]
        public void CancelOpenOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            pickingOrder.Release();

            //Act
            pickingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, pickingOrder.Status);
        }

        [Test]
        public void CancelCompletedOrder_DoesNotCancel()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            pickingOrder.Release();
            pickingOrder.Complete();

            //Act
            pickingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Completed, pickingOrder.Status);
        }

        [Test]
        public void CompleteOpenOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            pickingOrder.Release();

            //Act
            pickingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Completed, pickingOrder.Status);
        }

        [Test]
        public void CompleteDraftOrder_DoesNotComplete()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();

            //Act
            pickingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Draft, pickingOrder.Status);
        }

        [Test]
        public void CompleteCanceledOrder_DoesNotComplete()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            pickingOrder.Cancel();

            //Act
            pickingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, pickingOrder.Status);
        }

        [Test]
        public void AddItemToDraftPickingOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();

            //Act
            pickingOrder.AddLineItem(new Item("testItem"), 1);

            //Assert
            Assert.IsNotEmpty(pickingOrder.LineItems);
            Assert.AreEqual(1, pickingOrder.LineItems.Count);
        }

        [Test]
        public void AddItemToDraftPickingOrderThatOrderAlreadyHas_IncreaseQuantity()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");

            //Act
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.AddLineItem(item, 1);

            //Assert
            Assert.AreEqual(1, pickingOrder.LineItems.Count);
            Assert.AreEqual(2, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void AddItemSameToOpenPickingOrder_OrderUnaltered()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.Release();

            //Act
            pickingOrder.AddLineItem(item, 1);

            //Assert
            Assert.AreEqual(1, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void AddItemDifferentToOpenPickingOrder_OrderUnaltered()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item1 = new Item("testItem");
            var item2 = new Item("testItem");
            pickingOrder.AddLineItem(item1, 1);
            pickingOrder.Release();

            //Act
            pickingOrder.AddLineItem(item2, 1);

            //Assert
            Assert.AreEqual(1, pickingOrder.LineItems.Count);
        }

        [Test]
        public void AddItemToPickingOrder_NegativeQuantity_SetsZero()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");

            //Act
            pickingOrder.AddLineItem(item, -1);

            //Assert
            Assert.AreEqual(0, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void AddAdditionalQuantityToExtantItem()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");
            pickingOrder.AddLineItem(item, 1);

            //Act
            pickingOrder.IncreaseLineItemQuantity(item, 1);

            //Assert
            Assert.AreEqual(1, pickingOrder.LineItems.Count);
            Assert.AreEqual(2, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void SubtractQuantityFromExtantItem()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");
            pickingOrder.AddLineItem(item, 2);

            //Act
            pickingOrder.DecreaseLineItemQuantity(item, 1);

            //Assert
            Assert.AreEqual(1, pickingOrder.LineItems.Count);
            Assert.AreEqual(1, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void SubtractQuantityFromItem_MoreThanItemHas_QuantitySetToZero()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = new Item("testItem");
            pickingOrder.AddLineItem(item, 1);

            //Act
            pickingOrder.DecreaseLineItemQuantity(item, 2);

            //Assert
            Assert.AreEqual(0, pickingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void RemoveItemFromDraftPickingOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("testItem");
            pickingOrder.AddLineItem(item, 1);

            //Act
            pickingOrder.RemoveLineItem(item);

            //Assert
            Assert.IsEmpty(pickingOrder.LineItems);
            Assert.AreEqual(0, pickingOrder.LineItems.Count);
        }

        [Test]
        public void RemoveItemFromOpenPickingOrder_OrderUnaltered()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("testItem");
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.Release();

            //Act
            pickingOrder.RemoveLineItem(item);

            //Assert
            Assert.IsNotEmpty(pickingOrder.LineItems);
            Assert.AreEqual(1, pickingOrder.LineItems[item.AggregateId].Quantity);
        }
        #endregion

        #region Pick Activity Tests
        [Test]
        public void ShowLineUnpicked()
        {
            //Arrange & Act
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("TestItem");
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.Release();

            //Assert
            var lineItem = (PickingOrderLineItem)pickingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(0, lineItem.QuantityPicked);
            Assert.AreEqual(PickedStatus.Unpicked, lineItem.PickedStatus);
        }

        [Test]
        public void MarkLinePicked()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("TestItem");
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.Release();

            //Act
            pickingOrder.Pick(item.AggregateId, 1);

            //Assert
            var lineItem = (PickingOrderLineItem)pickingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(1, lineItem.QuantityPicked);
            Assert.AreEqual(PickedStatus.Picked, lineItem.PickedStatus);
        }

        [Test]
        public void MarkLinePartialPicked()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("TestItem");
            pickingOrder.AddLineItem(item, 2);
            pickingOrder.Release();

            //Act
            pickingOrder.Pick(item.AggregateId, 1);

            //Assert
            var lineItem = (PickingOrderLineItem)pickingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(1, lineItem.QuantityPicked);
            Assert.AreEqual(PickedStatus.PartialPicked, lineItem.PickedStatus);
        }

        [Test]
        public void MarkLineOverPicked()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item = Item.Create("TestItem");
            pickingOrder.AddLineItem(item, 1);
            pickingOrder.Release();

            //Act
            pickingOrder.Pick(item.AggregateId, 2);

            //Assert
            var lineItem = (PickingOrderLineItem)pickingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(2, lineItem.QuantityPicked);
            Assert.AreEqual(PickedStatus.OverPicked, lineItem.PickedStatus);
        }

        [Test]
        public void PickCompleteOrder()
        {
            //Arrange
            var pickingOrder = PickingOrder.Create();
            var item1 = Item.Create("TestItemOne", "The first test item");
            var item2 = Item.Create("TestItemTwo", "The second test item");
            pickingOrder.AddLineItem(item1, 1);
            pickingOrder.AddLineItem(item2, 2);
            pickingOrder.Release();

            //Act
            pickingOrder.PickComplete();

            //Assert
            var lineItem1 = (PickingOrderLineItem) pickingOrder.LineItems[item1.AggregateId];
            var lineItem2 = (PickingOrderLineItem)pickingOrder.LineItems[item2.AggregateId];
            Assert.AreEqual(1, lineItem1.QuantityPicked);
            Assert.AreEqual(PickedStatus.Picked, lineItem1.PickedStatus);
            Assert.AreEqual(2, lineItem2.QuantityPicked);
            Assert.AreEqual(PickedStatus.Picked, lineItem2.PickedStatus);
            Assert.AreEqual(OrderStatus.Completed, pickingOrder.Status);
        }
        #endregion
    }
}
