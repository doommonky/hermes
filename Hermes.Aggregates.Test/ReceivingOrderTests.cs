﻿using Hermes.Aggregates.Items;
using Hermes.Aggregates.Orders;
using Hermes.Aggregates.Statuses;
using NUnit.Framework;

namespace Hermes.Aggregates.Test
{
    [TestFixture]
    class ReceivingOrderTests
    {
        #region Create Picking Order Tests
        [Test]
        public void CreateReceivingOrder()
        {
            //Arrange & Act
            var receivingOrder = ReceivingOrder.Create();
            //Assert
            Assert.IsInstanceOf<ReceivingOrder>(receivingOrder);
        }

        [Test]
        public void WhenReceivingOrderIsCreated_StatusSetToDraft()
        {
            //Arrange & Act
            var receivingOrder = ReceivingOrder.Create();

            //Assert
            Assert.AreEqual(OrderStatus.Draft, receivingOrder.Status);
        }
        #endregion

        #region Modify Receiving Order Tests
        [Test]
        public void ReleaseOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();

            //Act
            receivingOrder.Release();

            //Assert
            Assert.AreEqual(OrderStatus.Open, receivingOrder.Status);
        }

        [Test]
        public void CancelDraftOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();

            //Act
            receivingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, receivingOrder.Status);
        }

        [Test]
        public void CancelOpenOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            receivingOrder.Release();

            //Act
            receivingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, receivingOrder.Status);
        }

        [Test]
        public void CancelCompletedOrder_DoesNotCancel()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            receivingOrder.Release();
            receivingOrder.Complete();

            //Act
            receivingOrder.Cancel();

            //Assert
            Assert.AreEqual(OrderStatus.Completed, receivingOrder.Status);
        }

        [Test]
        public void CompleteOpenOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            receivingOrder.Release();

            //Act
            receivingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Completed, receivingOrder.Status);
        }

        [Test]
        public void CompleteDraftOrder_DoesNotComplete()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();

            //Act
            receivingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Draft, receivingOrder.Status);
        }

        [Test]
        public void CompleteCanceledOrder_DoesNotComplete()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            receivingOrder.Cancel();

            //Act
            receivingOrder.Complete();

            //Assert
            Assert.AreEqual(OrderStatus.Canceled, receivingOrder.Status);
        }

        [Test]
        public void AddItemToDraftReceivingOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();

            //Act
            receivingOrder.AddLineItem(new Item("testItem"), 1);

            //Assert
            Assert.IsNotEmpty(receivingOrder.LineItems);
            Assert.AreEqual(1, receivingOrder.LineItems.Count);
        }

        [Test]
        public void AddItemToDraftReceivingOrderThatOrderAlreadyHas_IncreaseQuantity()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = new Item("testItem");

            //Act
            receivingOrder.AddLineItem(item, 1);
            receivingOrder.AddLineItem(item, 1);

            //Assert
            Assert.AreEqual(1, receivingOrder.LineItems.Count);
            Assert.AreEqual(2, receivingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void AddAdditionalQuantityToExtantItem()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = new Item("testItem");
            receivingOrder.AddLineItem(item, 1);

            //Act
            receivingOrder.IncreaseLineItemQuantity(item, 1);

            //Assert
            Assert.AreEqual(1, receivingOrder.LineItems.Count);
            Assert.AreEqual(2, receivingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void SubtractQuantityFromExtantItem()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = new Item("testItem");
            receivingOrder.AddLineItem(item, 2);

            //Act
            receivingOrder.DecreaseLineItemQuantity(item, 1);

            //Assert
            Assert.AreEqual(1, receivingOrder.LineItems.Count);
            Assert.AreEqual(1, receivingOrder.LineItems[item.AggregateId].Quantity);
        }

        [Test]
        public void RemoveItemFromDraftReceivingOrder()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = Item.Create("testItem");
            receivingOrder.AddLineItem(item, 1);

            //Act
            receivingOrder.RemoveLineItem(item);

            //Assert
            Assert.IsEmpty(receivingOrder.LineItems);
            Assert.AreEqual(0, receivingOrder.LineItems.Count);
        }
        #endregion

        #region Receiving Activity Tests

        [Test]
        public void ShowUnreceived()
        {
            //Arrange & Act
            var receivingOrder = ReceivingOrder.Create();
            var item = Item.Create("TestItem");
            receivingOrder.AddLineItem(item, 1);
            receivingOrder.Release();

            //Assert
            var lineItem = (ReceivingOrderLineItem) receivingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(0, lineItem.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.Unreceived, lineItem.ReceivedStatus);
        }

        [Test]
        public void MarkLineReceived()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = Item.Create("TestItem");
            receivingOrder.AddLineItem(item, 1);
            receivingOrder.Release();

            //Act
            receivingOrder.Receive(item.AggregateId, 1);

            //Assert
            var lineItem = (ReceivingOrderLineItem) receivingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(1, lineItem.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.Received, lineItem.ReceivedStatus);
        }

        [Test]
        public void MarkLinePartialReceived()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = Item.Create("TestItem");
            receivingOrder.AddLineItem(item, 2);
            receivingOrder.Release();

            //Act
            receivingOrder.Receive(item.AggregateId, 1);

            //Assert
            var lineItem = (ReceivingOrderLineItem)receivingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(1, lineItem.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.PartialReceived, lineItem.ReceivedStatus);
        }

        [Test]
        public void MarkLineOverReceived()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item = Item.Create("TestItem");
            receivingOrder.AddLineItem(item, 1);
            receivingOrder.Release();

            //Act
            receivingOrder.Receive(item.AggregateId, 2);

            //Assert
            var lineItem = (ReceivingOrderLineItem)receivingOrder.LineItems[item.AggregateId];
            Assert.AreEqual(2, lineItem.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.OverReceived, lineItem.ReceivedStatus);
        }

        [Test]
        public void ReceiveEntireOrderAndSetAsComplete()
        {
            //Arrange
            var receivingOrder = ReceivingOrder.Create();
            var item1 = Item.Create("TestItemOne");
            var item2 = Item.Create("TestItemTwo");
            receivingOrder.AddLineItem(item1, 1);
            receivingOrder.AddLineItem(item2, 2);
            receivingOrder.Release();

            //Act
            receivingOrder.ReceiveComplete();

            //Assert
            var lineItem1 = (ReceivingOrderLineItem)receivingOrder.LineItems[item1.AggregateId];
            var lineItem2 = (ReceivingOrderLineItem)receivingOrder.LineItems[item2.AggregateId];
            Assert.AreEqual(1, lineItem1.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.Received, lineItem1.ReceivedStatus);
            Assert.AreEqual(2, lineItem2.QuantityReceived);
            Assert.AreEqual(ReceivedStatus.Received, lineItem2.ReceivedStatus);
            Assert.AreEqual(OrderStatus.Completed, receivingOrder.Status);
        }
        #endregion
    }
}
