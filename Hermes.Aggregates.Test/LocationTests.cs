﻿using System;
using Hermes.Aggregates.Locations;
using NUnit.Framework;

namespace Hermes.Aggregates.Test
{
    [TestFixture]
    class LocationTests
    {
        #region Location Creation Tests

        [Test]
        public void CreateShelf()
        {
            //Arrange & Act
            var shelf = Location.Create("Top", "Shelf", "Bin");

            //Assert
            Assert.IsInstanceOf<Location>(shelf);
        }

        [Test]
        public void CreateLocation_IdIsSet()
        {
            //Arrange & Act
            var location = (Location) Location.Create("Location", "Shelf", "Bin");

            //Assert
            Assert.AreNotEqual(Guid.Empty, location.AggregateId);
        }

        [Test]
        public void CreateShelfWithTenBinMax()
        {
            //Arrange & Act
            var shelf = Location.Create("Top", "Shelf", "Bin", 10);

            //Assert
            Assert.AreEqual(10, shelf.MaxSubLocations);
        }
        #endregion

        #region Location Configuration Tests
        [Test]
        public void AddBinToShelf()
        {
            //Arrange
            var shelf = Location.Create("Top","Shelf", "Bin");
            var bin = Bin.Create("testBin");

            //Act
            shelf.AddLocationToSubLocations(bin);

            //Assert
            Assert.IsNotEmpty(shelf.SubLocations);
        }

        [Test]
        public void AddBin_MaxBinsAlreadyReached_ThrowsInvalidOperationException()
        {
            //Arrange
            var shelf = Location.Create("Top","Shelf", "Bin", 1);
            shelf.AddLocationToSubLocations(Bin.Create("extantBin"));

            //Act & Assert
            Assert.Throws<InvalidOperationException>(() => shelf.AddLocationToSubLocations(Bin.Create("garbageBin")));
        }

        [Test]
        public void RemoveBinFromShelf()
        {
            //Arrange
            var shelf = Location.Create("Top","Shelf", "Bin");
            var bin = Bin.Create("testBin");
            shelf.AddLocationToSubLocations(bin);

            //Act
            shelf.RemoveLocationFromSubLocations(bin.AggregateId);

            //Assert
            Assert.IsEmpty(shelf.SubLocations);
        }

        [Test]
        public void AddShelfToSite()
        {
            //Arrange
            var site = Location.Create("Warehouse B", "Site", "Shelf", 20);
            var shelf = Location.Create("A40", "Shelf", "Bin");

            //Act
            site.AddLocationToSubLocations((LocationBase)shelf);

            //Assert
            Assert.AreEqual(1, site.SubLocations.Count);
        }

        [Test]
        public void AddLocationToLocation_FailsIfContainsDoesNotMatchIncomingDescription()
        {
            //Arrange
            var site = Location.Create("Warehouse B", "Site", "Shelf", 20);
            var unit = Location.Create("A40", "Unit", "Bin");

            //Act
            site.AddLocationToSubLocations((LocationBase)unit);

            //Assert
            Assert.AreEqual(0, site.SubLocations.Count);
        }
        #endregion
    }
}
