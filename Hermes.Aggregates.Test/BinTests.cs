﻿using Hermes.Aggregates.Items;
using Hermes.Aggregates.Locations;
using NUnit.Framework;

namespace Hermes.Aggregates.Test
{
    [TestFixture]
    class BinTests
    {
        #region Create Bin Tests
        [Test]
        public void CreateBin()
        {
            var bin = Bin.Create("location0001");
            Assert.IsInstanceOf<Bin>(bin);
        }
        #endregion

        #region Assign and Unassign Tests
        [Test]
        public void AssignItemToBinTest()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");

            //Act
            bin.Assign(item);

            //Assert
            Assert.AreEqual("testItem", bin.Item.Name);
        }

        [Test]
        public void AssignItemToBin_SetInitialQuantity()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");

            //Act
            bin.Assign(item, 1);

            //Assert
            Assert.AreEqual(1, bin.Quantity);
        }

        [Test]
        public void AssignItemToBin_NoInitialQuantity_ItemAssignedButEmptyBin()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");

            //Act
            bin.Assign(item);

            //Assert
            Assert.AreEqual(bin.Item.Name, "testItem");
            Assert.AreEqual(0, bin.Quantity);
        }

        [Test]
        public void UnassignItemFromBin()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");
            bin.Assign(item, 100);

            //Act
            bin.Unassign();

            //Assert
            Assert.AreEqual(null, bin.Item);
            Assert.AreEqual(null, bin.Quantity);
        }
        #endregion

        #region Pick and Receive tests
        [Test]
        public void BinHas100_50Picked_AssertBinHas50()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");
            bin.Assign(item, 100);

            //Act
            bin.Pick(50);

            //Assert
            Assert.AreEqual(50, bin.Quantity);
        }

        [Test]
        public void BinHas50_100Received_AssertBinHas150()
        {
            //Arrange
            var bin = Bin.Create("location0001");
            var item = Item.Create("testItem");
            bin.Assign(item, 50);

            //Act
            bin.Receive(100);

            //Assert
            Assert.AreEqual(150, bin.Quantity);
        }
        #endregion
    }
}
