﻿using Hermes.Web.Controllers;
using Hermes.Web.Requests;
using Hermes.Aggregates.Items;
using Hermes.Core.DatabaseServices;
using NUnit.Framework;
using Moq;
using Microsoft.AspNetCore.Mvc;

namespace Hermes.Web.Tests.ControllerTests
{
    [TestFixture]
    public class ItemsControllerTest
    {
        private readonly IEventStoreService _mockEventStoreService;

        public ItemsControllerTest()
        {
            _mockEventStoreService = new Mock<IEventStoreService>().Object;
        }

        [Test]
        public void CreateItemApiRequest_ResultsIn200()
        {
            //Arrange
            var controller = new ItemsController(_mockEventStoreService);

            //Act
            var result = (OkObjectResult)controller.CreateItem(new CreateItemRequest {Name = "TestItem"}).Result;

            //Assert
            Assert.AreEqual(200, result.StatusCode);
        }

        [Test]
        public void CreateItemApiRequest_ReturnsNewItemData()
        {
            //Arrange
            var controller = new ItemsController(_mockEventStoreService);

            //Act
            var result = (ObjectResult)controller.CreateItem(new CreateItemRequest {Name = "TestItem", Description = "A Test Item"}).Result;
            var resultBody = (Item)result.Value;

            //Assert
            Assert.AreEqual("TestItem", resultBody.Name);
        }

        [Test]
        public void CreateItemApiRequest_NullData_ReturnsBadRequest()
        {
            //Arrange
            var controller = new ItemsController(_mockEventStoreService);

            //Act
            var result = (ObjectResult)controller.CreateItem(new CreateItemRequest()).Result;

            //Assert
            Assert.AreEqual(400, result.StatusCode);
        }

        [Test]
        public void InactivateItem_Returns200Ok()
        {
            //Arrange
            var controller = new ItemsController(_mockEventStoreService);
            var item = Item.Create("TestItem");

            //Act
            var result = (ObjectResult)controller.InactivateItem(item.AggregateId.ToString()).Result;

            //Assert
            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
