﻿namespace Hermes.Aggregates.Statuses

{
    public enum ItemStatus
    {
        Inactive,
        Active
    }

    public enum OrderStatus
    {
        Canceled,    
        Draft,
        Open,
        Completed,
    }

    public enum PickedStatus
    {
        Unpicked,
        PartialPicked,
        Picked,
        OverPicked
    }

    public enum ReceivedStatus
    {
        Unreceived,
        PartialReceived, 
        Received,
        OverReceived
    }
}
