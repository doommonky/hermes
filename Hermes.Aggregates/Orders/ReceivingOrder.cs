﻿using System;
using System.Collections.Generic;
using Hermes.Aggregates.Statuses;

namespace Hermes.Aggregates.Orders
{
    public class ReceivingOrder : OrderBase
    {
        #region Constructor
        protected ReceivingOrder()
        {
            Status = OrderStatus.Draft;
            LineItems = new Dictionary<Guid, IOrderLineItem>();
        }
        #endregion

        #region Create Methods
        public static ReceivingOrder Create()
        {
            return new ReceivingOrder();
        }
        #endregion

        #region Line Item Methods
        public IOrder Receive(Guid itemId, int quantity)
        {
            var lineItem = (ReceivingOrderLineItem)LineItems[itemId];
            lineItem.Receive(quantity);
            return this;
        }

        public IOrder ReceiveComplete()
        {
            foreach (var lineItemBase in LineItems.Values)
            {
                var lineItem = (ReceivingOrderLineItem) lineItemBase;
                lineItem.Receive(lineItem.Quantity);
            }
            this.Complete();
            return this;
        }
        #endregion
    }
}
