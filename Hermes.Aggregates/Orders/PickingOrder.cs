﻿using System;
using System.Collections.Generic;
using Hermes.Aggregates.Statuses;

namespace Hermes.Aggregates.Orders
{
    public class PickingOrder : OrderBase
    {
        #region Constructor
        protected PickingOrder()
        {
            Status = OrderStatus.Draft;
            LineItems = new Dictionary<Guid, IOrderLineItem>();
        }
        #endregion

        #region Create Methods
        public static PickingOrder Create()
        {
            return new PickingOrder();
        }
        #endregion

        #region Line Item Methods
        public IOrder Pick(Guid itemId, int quantity)
        {
            if (this.IsOpen())
            {
                PickingOrderLineItem lineItem = (PickingOrderLineItem)LineItems[itemId];
                lineItem.Pick(quantity);
            }

            return this;
        }

        public IOrder PickComplete()
        {
            if (this.IsOpen())
            {
                foreach (var lineItemBase in LineItems.Values)
                {
                    var lineItem = (PickingOrderLineItem)lineItemBase;
                    lineItem.Pick(lineItem.Quantity);
                }

                this.Complete();
            }

            return this;
        }
        #endregion
    }
}
