﻿using System.IO;
using Hermes.Aggregates.Items;
using Hermes.Aggregates.Statuses;

namespace Hermes.Aggregates.Orders
{ 
    public class PickingOrderLineItem : OrderLineItemBase
    {
        #region Properties

        public PickedStatus PickedStatus => PickedStatusFromQuantityPicked();
        public int QuantityPicked { get; private set; }
        #endregion

        #region Constructor
        public PickingOrderLineItem(Item item, int quantity)
        {
            Item = item;
            Quantity = quantity >= 0 ? quantity : 0;
        }
        #endregion

        #region Public Methods
        public void Pick(int quantity)
        {
            if (quantity >= 0)
                QuantityPicked += quantity;
        }
        #endregion

        #region Private Methods
        private PickedStatus PickedStatusFromQuantityPicked()
        {
            var differenceOfPickedToRequested = Quantity - QuantityPicked;

            if (differenceOfPickedToRequested == 0)
                return PickedStatus.Picked;

            if (differenceOfPickedToRequested == Quantity)
                return PickedStatus.Unpicked;

            if (differenceOfPickedToRequested < 0)
                return PickedStatus.OverPicked;

            if (differenceOfPickedToRequested > 0 && differenceOfPickedToRequested < Quantity)
                return PickedStatus.PartialPicked;

            throw new InvalidDataException("Math has broken.");
        }
        #endregion
    }
}
