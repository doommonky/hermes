﻿using Hermes.Aggregates.Items;

namespace Hermes.Aggregates.Orders
{
    public abstract class OrderLineItemBase : IOrderLineItem
    {
        public Item Item { get; protected set; }
        public int Quantity { get; protected set; }

        #region Public Methods
        public void Plus(int quantityIncrease)
        {
            if (quantityIncrease > 0)
                Quantity += quantityIncrease;
        }

        public void Minus(int quantityDecrease)
        {
            if (quantityDecrease > 0)
                Quantity = quantityDecrease <= Quantity ? Quantity - quantityDecrease : 0;
        }
        #endregion

    }
}
