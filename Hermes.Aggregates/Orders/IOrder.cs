﻿using System;
using System.Collections;
using System.Collections.Generic;
using Hermes.Aggregates.Items;
using Hermes.Aggregates.Statuses;

namespace Hermes.Aggregates.Orders
{
    public interface IOrder
    {
        OrderStatus Status { get; }
        IDictionary<Guid, IOrderLineItem> LineItems { get; }

        IOrder Release();
        IOrder Complete();
        IOrder Cancel();
        IOrder AddLineItem(Item item, int quantity);
        IOrder RemoveLineItem(Item item);
        IOrder IncreaseLineItemQuantity(Item item, int quantityToIncrease);
        IOrder DecreaseLineItemQuantity(Item item, int quantityToDecrease);
    }
}