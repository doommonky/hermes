﻿using System.IO;
using Hermes.Aggregates.Items;
using Hermes.Aggregates.Statuses;

namespace Hermes.Aggregates.Orders
{ 
    public class ReceivingOrderLineItem : OrderLineItemBase
    {
        #region Properties
        public ReceivedStatus ReceivedStatus => ReceivedStatusFromQuantityPicked();
        public int QuantityReceived { get; private set; }
        #endregion

        #region Constructor
        public ReceivingOrderLineItem(Item item, int quantity)
        {
            Item = item;
            Quantity = quantity;
        }
        #endregion

        #region Public Methods

        public void Receive(int quantity)
        {
            if (quantity >= 0)
                QuantityReceived += quantity;
        }
        #endregion

        #region Private Methods
        private ReceivedStatus ReceivedStatusFromQuantityPicked()
        {
            var differenceOfPickedToRequested = Quantity - QuantityReceived;

            if (differenceOfPickedToRequested == 0)
                return ReceivedStatus.Received;

            if (differenceOfPickedToRequested == Quantity)
                return ReceivedStatus.Unreceived;

            if (differenceOfPickedToRequested < 0)
                return ReceivedStatus.OverReceived;

            if (differenceOfPickedToRequested > 0 && differenceOfPickedToRequested < Quantity)
                return ReceivedStatus.PartialReceived;

            throw new InvalidDataException("Math has broken.");
        }
        #endregion
    }
}
