﻿using Hermes.Aggregates.Items;

namespace Hermes.Aggregates.Orders
{
    public interface IOrderLineItem
    {
        Item Item { get; }
        int Quantity { get; }

        void Plus(int quantityIncrease);
        void Minus(int quantityDecrease);
    }
}
