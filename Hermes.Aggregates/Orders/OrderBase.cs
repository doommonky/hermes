﻿using System;
using System.Collections.Generic;
using Hermes.Aggregates.Items;
using Hermes.Aggregates.Statuses;
using Hermes.Core;

namespace Hermes.Aggregates.Orders
{
    public abstract class OrderBase : Aggregate, IOrder
    {
        #region Properties
        public OrderStatus Status { get; protected set; }
        public IDictionary<Guid, IOrderLineItem> LineItems { get; protected set; }
        #endregion

        #region Status Methods
        public IOrder Release()
        {
            if (Status.Equals(OrderStatus.Draft))
            {
                UpdateStatus(OrderStatus.Open);
            }

            return this;
        }

        public IOrder Complete()
        {
            if (Status.Equals(OrderStatus.Open))
            {
                UpdateStatus(OrderStatus.Completed);
            }

            return this;
        }

        public IOrder Cancel()
        {
            if (Status.Equals(OrderStatus.Draft) || Status.Equals(OrderStatus.Open))
            {
                UpdateStatus(OrderStatus.Canceled);
            }

            return this;
        }
        #endregion

        #region Protected Methods 
        protected void UpdateStatus(OrderStatus requestedStatus)
        {
            Status = requestedStatus;
        }

        protected bool IsEditable()
        {
            if (Status == OrderStatus.Draft)
                return true;

            return false;
        }

        protected bool IsOpen()
        {
            if (Status == OrderStatus.Open)
                return true;

            return false;
        }
        #endregion

        #region Line Item Methods
        public IOrder AddLineItem(Item item, int quantity)
        {
            if (!this.IsEditable())
                return this;

            if (!LineItems.ContainsKey(item.AggregateId))
            {
                var orderType = GetType().ToString();
                OrderLineItemBase lineItem;
                switch (orderType)
                {
                    case "Hermes.Aggregates.Orders.PickingOrder":
                        lineItem = new PickingOrderLineItem(item, quantity);
                        break;
                    case "Hermes.Aggregates.Orders.ReceivingOrder":
                        lineItem = new ReceivingOrderLineItem(item, quantity);
                        break;
                    default: throw new InvalidOperationException("Attempted to add a line item to an unknown order type.");
                }
                LineItems.Add(item.AggregateId, lineItem);
            }
            else
                LineItems[item.AggregateId].Plus(quantity);

            return this;
        }

        public IOrder RemoveLineItem(Item item)
        {
            if (this.IsEditable())
                LineItems.Remove(item.AggregateId);

            return this;
        }

        public IOrder IncreaseLineItemQuantity(Item item, int quantityToIncrease)
        {
            if (this.IsEditable())
                LineItems[item.AggregateId].Plus(quantityToIncrease);

            return this;
        }

        public IOrder DecreaseLineItemQuantity(Item item, int quantityToDecrease)
        {
            if (this.IsEditable())
                LineItems[item.AggregateId].Minus(quantityToDecrease);

            return this;
        }
        #endregion
    }
}