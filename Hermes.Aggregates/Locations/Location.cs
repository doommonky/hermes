﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hermes.Aggregates.Locations
{
    public class Location : LocationBase
    {
        #region Constructor
        public Location(string label, string description, string contains, int maxSubLocations) 
            : base(label, description, contains, maxSubLocations)
        {
            SubLocations = new Dictionary<Guid, ILocation>(); 
        }
        #endregion

        #region Public Methods
        public static ILocation Create(string label, string description, string contains, int maxSubLocations = 1)
        {
            var location = new Location(label, description, contains, maxSubLocations);
            return location;
        }
        #endregion
    }
}
