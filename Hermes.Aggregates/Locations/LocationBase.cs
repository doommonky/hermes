﻿using System;
using System.Collections.Generic;
using Hermes.Core;

namespace Hermes.Aggregates.Locations
{
    public class LocationBase : Aggregate, ILocation
    {
        #region Properties
        public string Label { get; protected set; }
        public string Description { get; protected set; }
        public string Contains { get; set; }
        public IDictionary<Guid, ILocation> SubLocations { get; protected set; }
        public int MaxSubLocations { get; protected set; }
        #endregion

        #region Constructor
        public LocationBase(string label, string description, string contains, int maxSubLocations = 1)
        {
            Label = label;
            Description = description;
            Contains = contains;
            MaxSubLocations = maxSubLocations;
        }
        #endregion

        #region Public Methods
        public ILocation AddLocationToSubLocations(LocationBase subLocation)
        {
            if (SubLocations.Count >= MaxSubLocations)
                throw new InvalidOperationException($"This {Description} can only contain {MaxSubLocations} {Contains} and already is at capacity.");

            if (subLocation.Description == this.Contains)
                SubLocations.Add(subLocation.AggregateId, subLocation);

            return this;
        }

        public ILocation RemoveLocationFromSubLocations(Guid locationId)
        {
            SubLocations.Remove(locationId);
            return this;
        }
        #endregion
    }
}