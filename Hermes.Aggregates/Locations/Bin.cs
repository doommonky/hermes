﻿using Hermes.Aggregates.Items;

namespace Hermes.Aggregates.Locations
{
    public class Bin : LocationBase
    {
        #region Bin Properties
        public Item Item { get; private set; }
        public int? Quantity { get; private set; }
        #endregion

        #region Constructor

        protected Bin(string label) : base(label, "Bin", "Item", 0)
        {
            SubLocations = null;
        }
        #endregion

        #region Public Methods
        public static Bin Create(string label)
        {
            var bin = new Bin(label);
            return bin;
        }

        public Bin Assign(Item item, int quantity = 0)
        {
            Item = item;
            Quantity = quantity;
            return this;
        }

        public Bin Unassign()
        {
            Item = null;
            Quantity = null;
            return this;
        }

        public Bin Pick(int quantityToPick)
        {
            Quantity -= quantityToPick;
            return this;
        }

        public Bin Receive(int quantityToReceive)
        {
            Quantity += quantityToReceive;
            return this;
        }
        #endregion
    }
}
