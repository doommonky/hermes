﻿using System;
using System.Collections.Generic;
using Hermes.Core;

namespace Hermes.Aggregates.Locations
{
    public interface ILocation
    {
        IDictionary<Guid, ILocation> SubLocations { get; }
        int MaxSubLocations { get; }

        ILocation AddLocationToSubLocations(LocationBase subLocation);
        ILocation RemoveLocationFromSubLocations(Guid locationId);
    }
}