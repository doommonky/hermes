﻿using System;
using System.Text;
using EventStore.ClientAPI;
using Hermes.Aggregates.Statuses;
using Hermes.Core;
using Newtonsoft.Json;

namespace Hermes.Aggregates.Items
{
    public class Item : Aggregate
    {
        #region Item Properties
        public string Name { get; }
        public string Description { get; private set; }
        public ItemStatus Status { get; private set; }
        #endregion

        #region Constructor
        public Item (string name, string description = null)
        {
            Name = name;
            Status = ItemStatus.Active;
            Description = description;
        }
        #endregion

        #region Public Methods
        public static Item Create(string name, string description = null)
        {
            Item item = new Item(name, description);
            item.CreateItemCreatedEvent();

            return item; 
        }

        public Item Activate()
        {
            this.Status = ItemStatus.Active;
            this.CreateItemStatusChangedEvent();
            return this;
        }

        public Item Inactivate()
        {
            this.Status = ItemStatus.Inactive;
            this.CreateItemStatusChangedEvent();
            return this;
        }

        public Item UpdateItemDescription(string description)
        {
            if (description != null)
                this.Description = description;

            return this;
        }
        #endregion

        #region Private Methods
        private void CreateItemCreatedEvent()
        {
            var e = new EventData(Guid.NewGuid(), "Item Created", true,
                Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this)), new byte[] { });
            this.AddUncommittedEvent(e);
        }

        private void CreateItemStatusChangedEvent()
        {
            var e = new EventData(this.AggregateId, "Item Status Changed", true,
                Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this.Status)), new byte[] {});
            this.AddUncommittedEvent(e);
        }
        #endregion
    }
}
