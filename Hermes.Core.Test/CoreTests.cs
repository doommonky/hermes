﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Hermes.Core.DatabaseServices;
using NUnit.Framework;
using NUnit.Framework.Internal.Execution;

namespace Hermes.Core.Test
{
    [TestFixture]
    class CoreTests
    {
        private readonly IEventStoreService _eventStoreService;

        public CoreTests()
        {
            _eventStoreService = new EventStoreService();
        }

        #region Base Tests
        [Test]
        public void TestTester()
        {
            var result = true;
            Assert.AreEqual(true, result);
        }
        #endregion

        #region AggregateTests
        [Test]
        public void SetAggregateIdWhenNewAggregateCreated()
        {
            var aggregate = new Aggregate();

            Assert.AreNotEqual(Guid.Empty, aggregate.AggregateId);
        }
        #endregion

        [Test]
        public void SetAggregateUncommittedEvent()
        {
            //Arrange
            var aggregate = new Aggregate();

            //Act
            aggregate.AddUncommittedEvent(new EventData(Guid.NewGuid(), "testEvent", false,
                Encoding.UTF8.GetBytes("some data"),
                Encoding.UTF8.GetBytes("some metadata")));

            //Assert
            Assert.AreEqual(1, aggregate.UncommittedEvents.Count);
        }

        [Test]
        public void SetAggregateUncommittedEvents()
        {
            //Arrange
            var aggregate = new Aggregate();
            EventData[] events = {
                new EventData(Guid.NewGuid(), "testEvent1", false,
                    Encoding.UTF8.GetBytes("some data"),
                    Encoding.UTF8.GetBytes("some metadata")),
                new EventData(Guid.NewGuid(), "testEvent2", false,
                    Encoding.UTF8.GetBytes("some data"),
                    Encoding.UTF8.GetBytes("some metadata"))
            };

            //Act
            aggregate.AddUncommittedEvents(events);

            //Assert
            Assert.AreEqual(2, aggregate.UncommittedEvents.Count);
        }

        #region EventStoreTests
        #endregion
    }
}
