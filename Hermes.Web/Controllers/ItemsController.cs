﻿using System;
using System.Threading.Tasks;
using Hermes.Aggregates.Items;
using Hermes.Core;
using Hermes.Core.DatabaseServices;
using Hermes.Web.Requests;
using Microsoft.AspNetCore.Mvc;

namespace Hermes.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ItemsController
    {
        private readonly IEventStoreService _eventStoreService;
        public ItemsController(IEventStoreService eventStoreService)
        {
            _eventStoreService = eventStoreService;
        }

        [HttpPost, Route("items/createItem/")]
        public async Task<IActionResult> CreateItem(CreateItemRequest command)
        {
            if (command.Name == null)
                return new BadRequestObjectResult(command);

            var item = Item.Create(command.Name, command.Description);
            await _eventStoreService.AppendToStream(item);

            return new OkObjectResult(item);
        }

        [HttpPost, Route("items/InactivateItem{id}")]
        public async Task<IActionResult> InactivateItem(string id)
        {
            if (id == null)
                return new BadRequestObjectResult(id);

            var item = (Item)Aggregate.Hydrate(Guid.Parse(id));
            item.Inactivate();
            await _eventStoreService.AppendToStream(item);

            return new OkObjectResult(item);
        }

        [HttpGet, Route("items/getItem/{id}")]
        public IActionResult GetItem(string id)
        {
            var aggregate = new Aggregate {AggregateId = Guid.Parse(id)};
            var events = _eventStoreService.ReadStream(aggregate);

            return new OkObjectResult(events);
        }
    }
}