﻿namespace Hermes.Web.Requests
{
    public class CreateItemRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}