﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace Hermes.Core.DatabaseServices
{
    public interface IEventStoreService
    {
        IEventStoreConnection Connection { get; }
        bool Connected { get; }
        void Connect();
        void Disconnect();
        Task<WriteResult> AppendToStream(Aggregate aggregate);
        ResolvedEvent[] ReadStream(Aggregate aggregate);
    }
}