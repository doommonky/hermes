﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace Hermes.Core.DatabaseServices
{
    public class EventStoreService : IEventStoreService
    {
        #region Properties
        public IEventStoreConnection Connection { get; } = EventStoreConnection.Create(ConnectionSettings.Default, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1113));
        public bool Connected { get; private set; }
        #endregion

        #region Fields
        #endregion

        #region Constructor

        public EventStoreService()
        {
            SubscribeToConnectionEvents();
            Connect();
        }
        #endregion

        #region Public Methods
        public void Connect()
        {
            Connection.ConnectAsync().Wait();
        }

        public void Disconnect()
        {
            Connection.Close();
        }

        public async Task<WriteResult> AppendToStream(Aggregate aggregate)
        {
            var result = await Connection.AppendToStreamAsync(
                aggregate.AggregateId.ToString(), 
                ExpectedVersion.NoStream,
                aggregate.UncommittedEvents);
            return result;
        }

        public ResolvedEvent[] ReadStream(Aggregate aggregate)
        {
            var slice = Connection.ReadStreamEventsForwardAsync(aggregate.AggregateId.ToString(), StreamPosition.Start,
                1, false).Result;
            return slice.Events.ToArray();
        }
        #endregion

        #region PrivateMethods
        private void SubscribeToConnectionEvents()
        {
            Connection.Connected += OnConnection;
            Connection.Disconnected += OnDisconnection;
            Connection.Closed += OnConnectionClosed;
            Connection.ErrorOccurred += OnErrorOccurred;
            Connection.AuthenticationFailed += OnAuthenticationFailed;
        }

        private void OnConnection(object sender, ClientConnectionEventArgs e)
        {
            Console.WriteLine("EventStore Connected.");
            this.Connected = true;
        }

        private void OnDisconnection(object sender, ClientConnectionEventArgs e)
        {
            Console.WriteLine("EventStore Disconnected.");
            this.Connected = false;
        }

        private void OnConnectionClosed(object sender, ClientClosedEventArgs e)
        {
            Console.WriteLine("EventStore connection closed");
            this.Connected = false;
        }

        private void OnErrorOccurred(object sender, ClientErrorEventArgs e)
        {
            Console.WriteLine("EventStore had an error.");
            this.Connected = false;
        }

        private void OnAuthenticationFailed(object sender, ClientAuthenticationFailedEventArgs e)
        {
            Console.WriteLine("EventStore authentication failed.");
            this.Connected = false;
        }
        #endregion
    }
}
