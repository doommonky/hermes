﻿using System;
using System.Collections.Generic;
using EventStore.ClientAPI;
using Newtonsoft.Json;

namespace Hermes.Core
{
    public class Aggregate
    {
        #region Aggregate Properties
        public Guid AggregateId { get; set; }
        public int AggregateVersion { get; protected set; }
        [JsonIgnore]
        public ResolvedEvent[] PreviousEvents { get; protected set; }
        [JsonIgnore]
        public List<EventData> UncommittedEvents { get; }
        #endregion

        #region Fields
        #endregion

        #region Constructor
        public Aggregate()
        {
            AggregateId = Guid.NewGuid();
            AggregateVersion = 1;
            UncommittedEvents = new List<EventData>();
        }
        #endregion

        #region Public Methods
        public static Aggregate Hydrate(Guid AggregateId)
        {
            return new Aggregate();
        }

        public List<EventData> AddUncommittedEvents(params EventData[] events)
        {
            foreach (var e in events)
            {
                UncommittedEvents.Add(e);
            }

            return UncommittedEvents;
        }

        public List<EventData> AddUncommittedEvent (EventData e)
        {
            UncommittedEvents.Add(e);

            return UncommittedEvents;
        }
        #endregion
    }
}
